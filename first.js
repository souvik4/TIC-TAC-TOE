function startGame(){
    for(var i = 1; i <= 9; i++){
        clearBox(i);
    }
    
    if(Math.random() < 0.5)
    {
        document.turn = "O";
    }
    else{
        document.turn = "X";
    }

    document.winner = null;
    setMessage(document.turn + " gets to start.");
}

function setMessage(msg){
    document.getElementById("message").textContent = msg;
}

function nextMove(square) {
    if(document.winner != null){ //if document.winner is not null, this person already won the game
      setMessage(document.winner + " already won the game");
    } 
    else if(square.textContent == ""){ //if what's on the inner text is empty, make the next move, if it's not empty throw the message
    square.textContent = document.turn;
    switchTurn();
  } else {
    setMessage("That square is already used.")
    }
  }

  function switchTurn() {
    if(checkForWinner(document.turn))
    { //if we have a winner it would be true,  if we don't have a winner it will be false
      setMessage("Congratulations, " + document.turn + "! you win!"); 
      document.winner = document.turn; //show winner
    } 
    else if (checkForTie()){
        setMessage("It's a TIE! play again..");
    }
    else if(document.turn == "X")
    {
      document.turn = "O";
      setMessage("It's " + document.turn + "'s turn!");
    } 
    else 
    {
      document.turn = "X";
      setMessage("It's " + document.turn + "'s turn!");
    }
  }

  function checkForWinner(move) {
    var result = false;
    if (checkRow(1, 2, 3, move) ||checkRow(4, 5, 6, move) ||   //all the possible combinations that you can have in a row
        checkRow(7, 8, 9, move) ||checkRow(1, 4, 7, move) ||
        checkRow(2, 5, 8, move) ||checkRow(3, 6, 9, move) ||
        checkRow(1, 5, 9, move) ||checkRow(3, 5, 7, move))
        {
          result = true;
        }
        return result;
  }

  function checkRow(a, b, c, move) {      //see if those 3 squares matches a parrticular move or not
    var result = false;
    if(getBox(a) == move && getBox(b) == move && getBox(c) == move){ //if three x are in a row it is going to have a true result
      result = true;
    }
    return result;
  }

  function getBox(number){
      return document.getElementById("s" + number).textContent; //s1 or s2...
  }
  
  function clearBox(number) {
    document.getElementById("s" + number).textContent = "";
  }

  function  checkForTie(){
      for(var i=1; i<10; i++){
        if(getBox(i) == "")
        return false;
      }
      return true;
  }